import numpy as np
import matplotlib.pyplot as plt

a = np.array([1, 2, 3])

# exp() calculates exponent
print(np.exp(a))
print("original",a)

# log() calculates the natural log (base: e)
print(np.log(a))

# to calculate log (base: 10)
print(np.log10(a))
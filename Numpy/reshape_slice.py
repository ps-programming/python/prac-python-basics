import numpy as np

# 2D numpy array
a = np.array([(1, 2, 3), (4, 5, 6)])
print(a)
# reshape will change the shape of the array. Here we are changing the 2x3 array into a 3x2 array
a = a.reshape(3, 2)
print(a)
print()

# slicing
b = np.array([(1, 2, 3), (4, 5, 6)])
# prints value in second column of all the rows
print(b[0:, 2])
print()
# prints value in second column in first row only
print(b[0:1, 2])

import numpy as np

a = np.array([(1, 2, 3), (4, 5, 6)])
b = np.array([(2, 2, 2), (4, 4, 4)])
# matrix addition, subtraction, multiplication and division is done element wise.
print(a + b)
print(a * b)
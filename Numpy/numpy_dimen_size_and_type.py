import numpy as np

a = np.array([(1, 2, 3), (4, 5, 6)])
# ndim gives the dimension of the array
print(a.ndim)
# itemsize gives bytesize of each element
print(a.itemsize)
# dtype gives the datatype of elements
print(a.dtype)

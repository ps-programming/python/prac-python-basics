import numpy as np

a = np.array([(1, 2, 3), (4, 5, 6)])
# size gives the number of elements in the array.
print(a.size)
# shape gives the shape of the array i.e, how many rows, columns etc.
print(a.shape)

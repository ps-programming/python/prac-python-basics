import numpy as np

# 2D numpy array
a = np.array([(1, 2, 3), (4, 5, 6)])
print(a)

print(a.max())
print(a.min())
print(a.sum())
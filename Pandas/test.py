import pandas as pd

d1 = pd.DataFrame({"num":[1,2,3,4,5], "age":[2,3,4,5,6]}, index=[2000,2001,2003,2005,2004])
d2 = pd.DataFrame({"num":[1,2,3,4,5], "age":[2,3,4,5,6]}, index=[2000,2001,2003,2005,2004])
print(pd.merge(d1,d2, on=["age"]))
